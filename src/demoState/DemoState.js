import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    isLogin: false,
    username: "Si",
  };

  renderContent = () => {
    if (this.state.isLogin) {
      return (
        <div>
          <p>Hello {this.state.username}</p>{" "}
          <button onClick={this.handleLogout}>Đăng xuất</button>
        </div>
      );
    } else {
      return <button onClick={this.handleLogin}>Đăng nhâp</button>;
    }
  };
  handleLogin = () => {
    this.setState({
      isLogin: true,
    });
  };

  handleLogout = () => {
    this.setState({
      isLogin: false,
    });
  };
  render() {
    return <div>{this.renderContent()}</div>;
  }
}
