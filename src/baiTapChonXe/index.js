import React, { Component } from "react";

export default class BaiTapChonXe extends Component {
  state = {
    carOpitop: "./img/imgRedCar.jpg",
  };

  handleChangeOption = (option) => {
    this.setState({
      carOpitop: option,
    });
  };
  render() {
    return (
      <div className="container my-5">
        <div className="row">
          <img src={this.state.carOpitop} alt="" className="col-7" />

          <div className="col-5 ">
            <button
              onClick={() => {
                this.handleChangeOption("./img/imgRedCar.jpg");
              }}
              className="btn btn-danger mx-2 "
            >
              Red color
            </button>
            <button
              onClick={() => {
                this.handleChangeOption("./img/imgSilverCar.jpg");
              }}
              className="btn btn-secondary mx-2"
            >
              Silver color
            </button>
            <button
              onClick={() => {
                this.handleChangeOption("./img/imgBlackCar.jpg");
              }}
              className="btn btn-dark mx-2"
            >
              Black color
            </button>
          </div>
        </div>
      </div>
    );
  }
}
