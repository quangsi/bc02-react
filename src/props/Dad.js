import React, { Component } from "react";

export default class Dad extends Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}
