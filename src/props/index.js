import React, { Children, Component } from "react";
import Child from "./Child";
import Dad from "./Dad";

export default class DemoProps extends Component {
  state = {
    name: "Si",
    gender: "Male",
  };

  render() {
    return (
      <div>
        <Dad>
          <Child name={this.state.name} goiTinh={this.state.gender} />
        </Dad>
      </div>
    );
  }
}
