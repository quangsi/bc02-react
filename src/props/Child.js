import React, { Component } from "react";

export default class Child extends Component {
  render() {
    let { name, goiTinh } = this.props;
    return (
      <div>
        <span>Child</span>
        <p>Name {name} </p>
        <p>Gender: {goiTinh}</p>
      </div>
    );
  }
}
