import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/sanPham";

let initalState = {
  soLuong: 10,
  isGood: true,
};

export const sanPhamReducer = (state = initalState, action) => {
  console.log(action);
  // 1
  // 2
  // 3
  let cloneState = { ...state };

  switch (action.type) {
    case TANG_SO_LUONG:
      cloneState.soLuong = cloneState.soLuong + action.payload;
      return cloneState;
    case GIAM_SO_LUONG:
      cloneState.soLuong = cloneState.soLuong - action.payload;
      return cloneState;
    default:
      break;
  }
  return { ...state };
};
