import React, { Component } from "react";

import { connect } from "react-redux";
import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./redux/constants/sanPham";

class DemoMiniRedux extends Component {
  render() {
    return (
      <>
        <div className="container py-5">
          <button
            className="btn btn-success"
            onClick={() => {
              this.props.tangSoLuong(5);
            }}
          >
            Tăng 1
          </button>

          <span className="mx-3">Số lượng: {this.props.soLuongOrder}</span>

          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.giamSoLuong(5);
            }}
          >
            Giảm 1
          </button>
        </div>
        <p>Ngon không {this.props.isGood ? "Ngon" : "Dỡ"}</p>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    soLuongOrder: state.sanPhamReducer.soLuong,
    isGood: state.sanPhamReducer.isGood,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (soLuong) => {
      dispatch({
        type: TANG_SO_LUONG,
        payload: soLuong,
      });
    },
    giamSoLuong: (soLuong) => {
      dispatch({
        type: GIAM_SO_LUONG,
        payload: soLuong,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);

//  connet ()  ()
