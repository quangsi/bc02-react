import React, { Component } from "react";

export default class DemoIfElse extends Component {
  isLogin = false;
  username = "Sĩ";
  renderContent = () => {
    if (this.isLogin) {
      return "Hello" + this.username;
    } else {
      return <button onClick={this.hanleLogin}>Đăng nhập</button>;
    }
  };

  hanleLogin = () => {
    this.isLogin = true;
    console.log(this.isLogin);
  };

  render() {
    return <div>{this.renderContent()}</div>;
  }
}
