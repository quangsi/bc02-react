import logo from "./logo.svg";
import "./App.css";
import DemoDieuKien from "./demo-dieu-kien/DemoDieuKien";
import DemoState from "./demoState/DemoState";
import BaiTapChonXe from "./baiTapChonXe";
import RenderWithMap from "./renderWithMap";
import DemoProps from "./props";
import LiftingStateUp from "./lifting-state-up";
import BaiTapGioHang from "./BaiTapGioHang";
import "antd/dist/antd.css";
import DemoMiniRedux from "./demoMiniRedux";
function App() {
  return (
    <div className="">
      {/* <DemoDieuKien /> */}
      {/* <DemoState /> */}
      {/* <BaiTapChonXe /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoProps /> */}
      {/* <LiftingStateUp /> */}
      {/* <BaiTapGioHang /> */}
      <DemoMiniRedux />
    </div>
  );
}

export default App;
