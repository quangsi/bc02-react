import React, { Component } from "react";

export default class ItemDienThoai extends Component {
  render() {
    let { tenSP, hinhAnh } = this.props.data;
    let { handleOnclick } = this.props;
    return (
      <div className="card" style={{ width: "25%" }}>
        <img className="card-img-top" src={hinhAnh} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{tenSP}</h5>

          <button
            onClick={() => {
              handleOnclick(this.props.data);
              //   console.log("yesss");
            }}
            className="btn btn-primary"
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    );
  }
}
