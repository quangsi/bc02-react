import React, { Component } from "react";
import DanhSachDienThoai from "./DanhSachDienThoai";
import { danhSachDienThoai } from "./data";
import SanPhamChiTiet from "./SanPhamChiTiet";

export default class LiftingStateUp extends Component {
  state = {
    sanPhamChiTiet: {
      maSP: 3,
      tenSP: "Iphone XS Max",
      manHinh: "OLED, 6.5, 1242 x 2688 Pixels",
      heDieuHanh: "iOS 12",
      cameraSau: "Chính 12 MP & Phụ 12 MP",
      cameraTruoc: "7 MP",
      ram: "4 GB",
      rom: "64 GB",
      giaBan: 27000000,
      hinhAnh: "./img/applephone.jpg",
    },
  };

  handleSanPhamChiTiet = (sanPham) => {
    console.log(sanPham);
    this.setState({
      sanPhamChiTiet: sanPham,
    });
  };
  render() {
    return (
      <div>
        <DanhSachDienThoai
          dsdt={danhSachDienThoai}
          handleSanPhamChiTiet={this.handleSanPhamChiTiet}
        />
        <SanPhamChiTiet data={this.state.sanPhamChiTiet} />
      </div>
    );
  }
}
