import React, { Component } from "react";

export default class ItemDienThoai extends Component {
  render() {
    // let { url, name, price } = this.props?.data;
    return (
      <div
        style={{
          width: "300px",
        }}
      >
        <img src={this.props.data?.url} alt="" />
        <p className="text-red-500">Tên địa thoạii : {this.props.data?.name}</p>
        <span>Giá tiền: {this.props.data?.price}</span>
      </div>
    );
  }
}
