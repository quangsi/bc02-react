import React, { Component } from "react";
import { dsst } from "./dataDsDienthoai";
import ItemDienThoai from "./ItemDienThoai";

export default class RenderWithMap extends Component {
  state = {
    data: dsst,
  };

  renderData = () => {
    return this.state.data.map((item) => {
      return <ItemDienThoai data={item} />;
    });
  };
  render() {
    return <div style={{ display: "flex" }}>{this.renderData()}</div>;
  }
}
